options:
  docker: true

pipelines:
  branches:
    master:
      - step:
          name: Infrastructure 1 ECR
          script:
            # install AWS CLI
            - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            - unzip awscliv2.zip
            - sudo ./aws/install
            - export BUILD_ID=Dummy
            - pipe: atlassian/aws-cloudformation-deploy:0.6.1
              variables:
                AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
                AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}
                AWS_DEFAULT_REGION: ${AWS_DEFAULT_REGION}
                STACK_NAME: ${STACK_NAME}-ecr
                TEMPLATE: "cicd/ecr.yaml"
                CAPABILITIES: ['CAPABILITY_IAM']
                WAIT: 'true'
                STACK_PARAMETERS: >
                  [{
                    "ParameterKey": "RepositoryName",
                    "ParameterValue": "${AWS_REGISTRY_NAME}"
                  }]
            - export AWS_REGISTRY_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${AWS_REGISTRY_NAME}
            - docker pull amazon/aws-lambda-nodejs:12
            - docker tag amazon/aws-lambda-nodejs:12 ${AWS_REGISTRY_URL}:$BUILD_ID
            - aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.eu-west-1.amazonaws.com
            - docker push ${AWS_REGISTRY_URL}:$BUILD_ID

      - step:
          name: Infrastructure 2 vpc ecs
          script:
            # install AWS CLI
            - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            - unzip awscliv2.zip
            - sudo ./aws/install
            - export AWS_REGISTRY_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${AWS_REGISTRY_NAME}
            - export BUILD_ID=Dummy
            - pipe: atlassian/aws-cloudformation-deploy:0.6.1
              variables:
                AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
                AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
                AWS_DEFAULT_REGION: ${AWS_DEFAULT_REGION}
                STACK_NAME: ${STACK_NAME}-app
                TEMPLATE: "cicd/infrastructure.yaml"
                CAPABILITIES: [ 'CAPABILITY_NAMED_IAM' ]
                WAIT: 'true'
                STACK_PARAMETERS: >
                  [
                  {
                    "ParameterKey": "EnvironmentName",
                    "ParameterValue": "${ENVIRONMENT}"
                  },
                  {
                    "ParameterKey": "VpcCIDR",
                    "ParameterValue": "${VPC_CIDR}"
                  },
                  {
                    "ParameterKey": "PublicSubnetOneCIDR",
                    "ParameterValue": "${PUBLIC_SUBNET_1_CIDR}"
                  },
                  {
                    "ParameterKey": "PublicSubnetTwoCIDR",
                    "ParameterValue": "${PUBLIC_SUBNET_2_CIDR}"
                  },
                  {
                    "ParameterKey": "PrivateSubnetOneCIDR",
                    "ParameterValue": "${PRIVATE_SUBNET_1_CIDR}"
                  },
                  {
                    "ParameterKey": "PrivateSubnetTwoCIDR",
                    "ParameterValue": "${PRIVATE_SUBNET_2_CIDR}"
                  },
                  {
                    "ParameterKey": "RepositoryUrl",
                    "ParameterValue": "${AWS_REGISTRY_URL}:${BUILD_ID}"
                  },
                  {
                    "ParameterKey": "ContainerPort",
                    "ParameterValue": "${ECS_CONTAINER_PORT}"
                  },
                  {
                    "ParameterKey": "ServiceName",
                    "ParameterValue": "${ECS_SERVICE_NAME}"
                  },
                  {
                    "ParameterKey": "LoadBalancerPort",
                    "ParameterValue": "${LOAD_BALANCER_PORT}"
                  },
                  {
                    "ParameterKey": "HealthCheckPath",
                    "ParameterValue": "${HEALTH_CHECK_PATH}"
                  },
                  {
                    "ParameterKey": "DesiredCount",
                    "ParameterValue": "0"
                  },
                  {
                    "ParameterKey": "CkusterName",
                    "ParameterValue": "${STACK_NAME}"
                  }]

      - step:
          name: Build and push
          script:
            # install AWS CLI
            - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            - unzip awscliv2.zip
            - sudo ./aws/install
            # aws ecr repo login
            - aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.eu-west-1.amazonaws.com
            # docker build and push
            - export AWS_REGISTRY_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${AWS_REGISTRY_NAME}
            - export BUILD_ID=$BITBUCKET_BRANCH_$BITBUCKET_COMMIT_$BITBUCKET_BUILD_NUMBER
            - docker build -t ${AWS_REGISTRY_URL}:$BUILD_ID .
            - docker push ${AWS_REGISTRY_URL}:$BUILD_ID

      - step:
          name: Deploy
          image: tstrohmeier/awscli:3.8.3
          script:
            # Create task definition with build image
            - export AWS_REGISTRY_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${AWS_REGISTRY_NAME}
            - export BUILD_ID=$BITBUCKET_BRANCH_$BITBUCKET_COMMIT_$BITBUCKET_BUILD_NUMBER
            - export ECR_IMAGE=${AWS_REGISTRY_URL}:${BUILD_ID}
            - export TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "${ECS_SERVICE_NAME}" --region "${AWS_DEFAULT_REGION}")
            - export NEW_TASK_DEFINTIION=$(echo $TASK_DEFINITION | jq --arg IMAGE "${ECR_IMAGE}" '.taskDefinition | .containerDefinitions[0].image = $IMAGE | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities)')
            - export NEW_TASK_INFO=$(aws ecs register-task-definition --region "${AWS_DEFAULT_REGION}" --cli-input-json "$NEW_TASK_DEFINTIION")
            - export NEW_REVISION=$(echo $NEW_TASK_INFO | jq '.taskDefinition.revision')
            - aws ecs update-service --cluster ${STACK_NAME} --service ${ECS_SERVICE_NAME} --task-definition "${ECS_SERVICE_NAME}" --desired-count ${DESIRED_TASK_COUNT}

      - step:
          name: Refresh
          trigger: manual
          script:
            # install AWS CLI
            - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            - unzip awscliv2.zip
            - sudo ./aws/install

            # Refresh Infrastructure
            - export IMAGES_TO_DELETE=${aws ecr list-images --region AWS_DEFAULT_REGION --repository-name ${AWS_REGISTRY_NAME} --query 'imageIds[*]' --output json}
            - aws ecr batch-delete-image --region ${AWS_DEFAULT_REGION} --repository-name ${AWS_REGISTRY_NAME} --image-ids "$IMAGES_TO_DELETE" || true
            - aws ecs update-service --cluster ${STACK_NAME} --service ${ECS_SERVICE_NAME} --task-definition "${ECS_SERVICE_NAME}" --desired-count 0

      - step:
          name: Destroy
          trigger: manual
          script:
            # install AWS CLI
            - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            - unzip awscliv2.zip
            - sudo ./aws/install

            # Destroty
            - aws ecr delete-repository --repository-name ${AWS_REGISTRY_NAME} --force --region ${AWS_DEFAULT_REGION}
            - aws cloudformation delete-stack --stack-name ${STACK_NAME}-ecr --region ${AWS_DEFAULT_REGION}
            - aws cloudformation delete-stack --stack-name ${STACK_NAME}-app --region ${AWS_DEFAULT_REGION}

